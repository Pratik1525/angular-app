const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('D:/AngularApp/angular-app/server/db/user.db', sqlite3.OPEN_READWRITE, function(err) {
    if (err) {
      console.error("database error:",err.message);
    }
    else{
      createTables();
      console.log('database connection success.');
    }  
});

function createTables(){    
    db.run("CREATE TABLE if not exists userInfo (userId TEXT, name TEXT NOT NULL, surname TEXT NOT NULL, email text PRIMARY KEY,city text  NOT NULL,gender text NOT NULL,skills text NOT NULL)");
    db.run("CREATE TABLE if not exists cities (cityId TEXT PRIMARY KEY, city TEXT NOT NULL)");
    db.run("CREATE TABLE if not exists skills (skillId TEXT PRIMARY KEY, skill TEXT NOT NULL)");
}

module.exports = {
    db
}
