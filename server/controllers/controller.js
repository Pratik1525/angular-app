
const {db} = require('../common/connection');

const getCities = (req, res) => {

    let data = {}
   
    db.all("SELECT city FROM cities", function(err,cities){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            data.cities = cities;
            return  res.json({response: data,  msg: "success", err:null});
        }            
    }) 
}

const createUser = (req, res) => {

    console.log("createUser",req)
    
    let {userId,name,surname,email,city,gender,skills} = req.body
    db.run("insert into userInfo VALUES (?,?,?,?,?,?,?)",[userId,name,surname,email,city,gender,skills], function(err,data){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            // let data={};
            // db.all("SELECT * FROM userInfo", function(err,users){
            //     if(err){
            //         return  res.json({response: null,  msg: "error", err:err})
            //     }     
            //     else{
            //         data.users = users;
            //         return  res.json({response: data,  msg: "success", err:null});
            //     }            
            // }) 
            listUsers(req,res);
        }            
    }) 
}

const listUsers = (req, res) => {

    let data = {}
   
    db.all("SELECT * FROM userInfo", function(err,users){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            data.users = users;
            return  res.json({response: data,  msg: "success", err:null});
        }            
    }) 
}

const deleteUser = (req, res) => {

    let data = {}
   
    db.run("delete from userInfo where userId="+req.body.userId, function(err,users){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            listUsers(req,res);
        }            
    }) 
}

const editUser = (req, res) => {

    let data = {}
    let {userId,name,surname,email,city,gender,skills} = req.body
    db.run(`update userInfo set (name,surname,email,city,gender,skills)  = (${name},${surname},${email},${city},${gender},${skills}) where userId=${userId}`, function(err,user){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            data.user = user;
            return  res.json({response: data,  msg: "success", err:null});
        }            
    }) 
}

const getUser = (req, res) => {
    let data = {}    
    let userId = req.body.userId;
    let query = "SELECT * FROM userInfo where userId='"+userId+"';"
    db.all(query, function(err,user){
        if(err){
            return  res.json({response: null,  msg: "error", err:err})
        }     
        else{
            data.user = user;
            return  res.json({response: data,  msg: "success", err:null});
        }            
    }) 
}

module.exports = {
    getCities,
    createUser,
    listUsers,
    deleteUser,
    editUser,
    getUser
}