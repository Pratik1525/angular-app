const express = require('express');
const cors = require('cors');
const bodyParser =require('body-parser');
const route = require('./routes/routes');
const fileUpload = require('express-fileupload');
const app = express();
const port = process.env.PORT || 5000;
const http = require('http');
const server = http.createServer(app);
var db = require('./common/connection')

app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use(cors());

app.use('/api', route);

server.listen(port, function () {
    console.log(`server started at ${port}`);
}
);