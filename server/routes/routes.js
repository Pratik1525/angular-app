var express = require('express');
var router = express.Router();
var controller = require('../controllers/controller');
/* GET home page. */
router.get('/xyz', function(req, res, next) {
  res.send('Express RESTful API');
});


router.get('/getcities',controller.getCities);
router.post('/createuser',controller.createUser);
router.get('/listusers',controller.listUsers);
router.post('/deleteuser',controller.deleteUser);
router.post('/edituser',controller.editUser);
router.post('/getuser',controller.getUser);
module.exports = router;