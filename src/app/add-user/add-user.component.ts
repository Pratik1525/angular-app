import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {User} from '../user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private api: ApiService,private router: Router) { }
  cities:string[]
  model:User
  viewMode = 'tab1'
  skills=['c#','Angular','React','node'];
  userskills:string[] = []

  ngOnInit() {
    this.model= new User(`${+new Date()}`,"","","","","","");
    this.api.getCities()
    .subscribe(res => {
      console.log('getCities response',res);
      this.cities = res.response.cities;
    }, err => {
      console.log(err);
    });
  }

  userSkills(skill){
    if(this.userskills.indexOf(skill) == -1){
      this.userskills.push(skill);
    }
    else{
      let index = this.userskills.indexOf(skill);
      this.userskills.splice(index,0);
    }

  }

  createuser(){
    this.model.skills = this.userskills.join(';');
    this.api.createUser(this.model)
    .subscribe(res => {
      if(res && res.err && res.err.errno==19){
        alert("email id should be unique")
      }
      else{
        this.model= new User(`${+new Date()}`,"","","","","","");
      }
      console.log('getCities response',res);
      this.cities = res.response.cities;
      this.router.navigate(["/","list-user",]).then(()=>{
        // do whatever you need after navigation succeeds
      });
    }, err => {
      console.log(err);
    });
  }

  fileChangeEvent(event){

    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      const headers = new Headers();
      headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', 'application/json');
      const body = JSON.stringify({ headers: headers });
   
    //   this.api.fileChangeEvent(this.model)
    // .subscribe(res => {
    //   if(res && res.err.errno==19){
    //     alert("email id should be unique")
    //   }
    //   else{
    //     this.model= new User(`${+new Date()}`,"","","","","","");
    //   }
    //   console.log('getCities response',res);
    //   this.cities = res.response.cities;
    // }, err => {
    //   console.log(err);
    // });

  }
  }
}
