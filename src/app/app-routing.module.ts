import { NgModule } from '@angular/core';  
import { CommonModule } from '@angular/common';  
import { RouterModule, Routes } from '@angular/router';  
import { ListUsersComponent } from './list-users/list-users.component';  
import { AddUserComponent } from './add-user/add-user.component';  
  
export const routes: Routes = [  
  { path: '', component: ListUsersComponent, pathMatch: 'full' },  
  { path: 'list-user', component: ListUsersComponent },  
  { path: 'add-user', component: AddUserComponent }  
];  
  
@NgModule({  
  imports: [  
    CommonModule,  
    RouterModule.forRoot(routes)  
  ],  
  exports: [RouterModule],  
  declarations: []  
})  
export class AppRoutingModule { }  