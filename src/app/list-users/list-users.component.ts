import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {User} from '../user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  constructor(private api: ApiService,private router: Router) { }
  userslist:any
  ngOnInit() {
    this.listUsers()
  }

  listUsers(){   
    this.api.listUsers()
    .subscribe(res => {
      this.userslist = res.response.users;
      console.log('this.userslist',this.userslist);
    }, err => {
      console.log(err);
    });
  }

  deleteUser(userId){
    this.api.deleteUser(userId)
    .subscribe(res => {
      console.log('this.userslist',this.userslist);
      location.reload();
      this.router.navigate(["/","list-user",])
    }, err => {
      console.log(err);
    });
  }

  getUser(userId){
    this.api.getUser(userId)
    .subscribe(res => {
      console.log('this.userslist',this.userslist);
    }, err => {
      console.log(err);
    });

  }
  editUser(userId){ 
    this.api.getUser(userId)
    .subscribe(res => {
      console.log('this.userslist',this.userslist);
    }, err => {
      console.log(err);
    });
  }
}
