export class User {
    constructor(
        public userId: string,
        public name: string,
        public surname: string,
        public email: string,
        public gender: string,
        public city: string,
        public skills:string
      ) {  }
}
